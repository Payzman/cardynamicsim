#include "./CarDynamicSimTest.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

TEST(CoordinateSystem, DefaultConstructor) {
    CoordinateSystem cs;
    ASSERT_TRUE(cs.getTranslation(Direction::X) == 0);
    ASSERT_TRUE(cs.getTranslation(Direction::Y) == 0);
    ASSERT_TRUE(cs.getTranslation(Direction::Z) == 0);
}

TEST(CoordinateSystem, SetGetTranslation) {
    CoordinateSystem cs;
    cs.setTranslation(Direction::X, 1);
    cs.setTranslation(Direction::Y, 2);
    cs.setTranslation(Direction::Z, 3);
    ASSERT_TRUE(cs.getTranslation(Direction::X) == 1);
    ASSERT_TRUE(cs.getTranslation(Direction::Y) == 2);
    ASSERT_TRUE(cs.getTranslation(Direction::Z) == 3);
}

TEST(CoordinateSystem, SetGetRotation) {
    CoordinateSystem cs;
    cs.setRotation(Rotation::Roll, 0.5);
    cs.setRotation(Rotation::Pitch, 0.6);
    cs.setRotation(Rotation::Yaw, 0.7);
    ASSERT_TRUE(cs.getRotation(Rotation::Roll) == 0.5);
    ASSERT_TRUE(cs.getRotation(Rotation::Pitch) == 0.6);
    ASSERT_TRUE(cs.getRotation(Rotation::Yaw) == 0.7);
}

TEST(CoordinateSystem, equality) {
    CoordinateSystem cs1;
    CoordinateSystem cs2;
    ASSERT_TRUE(cs1 == &cs2);
}

TEST(CoordinateSystem, inequality) {
    CoordinateSystem cs1;
    CoordinateSystem cs2;
    cs2.setRotation(Rotation::Roll, 0.5);
    ASSERT_TRUE(cs1 != &cs2);
}

TEST(Coordinate, SetGetValue) {
    Coordinate tc;
    ASSERT_TRUE(tc == 0.0);
    tc = 22.45;
    ASSERT_TRUE(tc == 22.45);
}

TEST(Wheel, DefaultConstructor) {
    Wheel wl;
    CoordinateSystem cs;
    ASSERT_TRUE(*(wl.getCoordinateSystem()) == &cs);
    cs.setRotation(Rotation::Yaw, 0.5);
    ASSERT_TRUE(*(wl.getCoordinateSystem()) != &cs);
}

TEST(Car, Wheels) {
    Car car;
    ASSERT_TRUE(car.getAmountWheels() == 4);
}


#pragma clang diagnostic pop
