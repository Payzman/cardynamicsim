#ifndef WHEEL_H_
#define WHEEL_H_

#include "./CoordinateSystem.hpp"

/*! \brief represents a single Wheel with a Coordinate System */
class Wheel {
    private:
        /*! \brief CoordinateSystem of the wheel */
        CoordinateSystem _cs;
    public:
        /*! \brief ability to use and change the coordinate system */
        CoordinateSystem* getCoordinateSystem();
};


#endif // WHEEL_H_
