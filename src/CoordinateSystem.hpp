#ifndef COORDINATESYSTEM_H_
#define COORDINATESYSTEM_H_

#include <unordered_map>

#include "./Direction.hpp"
#include "./Rotation.hpp"
#include "./RotationalCoordinate.hpp"
#include "./TranslationalCoordinate.hpp"

/*! \brief defines a coordinatesystem in 3D-Space.
 *
 */
class CoordinateSystem {
    private:
        /*! \brief defines the three translational axis
         *
         * The meaning of x, y and z (1 2 and 3) are dependent on the usecase.
         */
        std::unordered_map<Direction, TranslationalCoordinate> _translation;
        /*! \brief defines the three rotations around the translational axis.
         *
         * The meaning of roll, pitch and yaw are dependent on the usecase.
         */
        std::unordered_map<Rotation, RotationalCoordinate> _rotation;
    public:
        /*! \brief returns the value of one translational axis
         *
         * The meaning of x, y and z (1 2 and 3) are dependent on the usecase.
         * This function returns the single position of one of the axis.
         */
        TranslationalCoordinate getTranslation(Direction dir);
        /*! \brief returns the value of one rotational axis
         *
         * The meaning of roll, pitch and yaw are dependent on the usecase.
         * This method returns the single rotation around one of the axis.
         */
        RotationalCoordinate getRotation(Rotation rot);
        /*! \brief sets the value of one translational axis */
        void setTranslation(Direction dir, double val);
        /*! \brief sets the value of one rotation around an axis */
        void setRotation(Rotation rot, double val);
        /*! \brief checks for equality with another CoordinateSystem */
        bool operator==(CoordinateSystem* other);
        /*! \brief checks for inequality with another CoordinateSystem */
        bool operator!=(CoordinateSystem* other);
};

#endif // COORDINATESYSTEM_H_
