#ifndef DIRECTION_H_
#define DIRECTION_H_

/*! \brief defines the possible direction in 3 dimensional space. */
enum class Direction {
    X,
    Y,
    Z
};

#endif // DIRECTION_H_
