#ifndef ROTATION_H_
#define ROTATION_H_

/*! \brief represents the different rotation around different axis.
 *
 * - Pitch is the rotation around the x-axis.
 * - Roll is the rotation around the y-axis.
 * - Yaw is the rotation around the z-axis.
 */
enum class Rotation {
    Pitch,
    Roll,
    Yaw
};

#endif // ROTATION_H_
