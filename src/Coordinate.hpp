#ifndef COORDINATE_H_
#define COORDINATE_H_

#include <cmath>

/*! \brief defines a single translational coordinate within a coordinate system.
 *
 * This class is a wrapper for "double" and makes sure, that parameters for
 * functions etc. are actually used correctly.
 */
class Coordinate {
    private:
        /*! \brief holds the value of a point in space for this axis.*/
        double _s;
        /*! \brief defines the allowed approximation error for the == operator
         */
        static constexpr double epsilon = 0.0e-9;
    public:
        /*! \brief default constructor, sets value to 0.0 */
        Coordinate();
        /*! \brief converts a double into the Coordinate
         *
         * \param s is the double to be converted
         */
        Coordinate(double s);
        /*! \brief converts the Coordinate into a double value */
        Coordinate& operator=(double rhs);
        /*! \brief compares Coordinate with a double value */
        bool operator==(double other);
        /*! \brief compares Coordinate with another Coordinate */
        bool operator==(Coordinate other) const;
};

#endif // COORDINATE_H_
