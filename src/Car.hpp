#ifndef CAR_H_
#define CAR_H_

#include "./Coordinate.hpp"
#include "./CoordinateSystem.hpp"
#include "./Wheel.hpp"

/*! \brief Representation of a car including all the possible dynamic movements
 *
 * This class defines the cars coordinate system with \f$x\f$ defining the
 * longitudial axis for forwards and backwards movement, \f$y\f$ defining the
 * transverse axis for pushing left and right, for example due to winds and a
 * vertical axis \f$z\f$ for lifting movement due to uneven road surfaces.
 */
class Car {
    private:
        /*! \brief defines the coordinate system for the chassis
         *
         * The X-Direction represents the longitudial movement of the cars body,
         * especially by accelerating and delecerating. The Y-Direction
         * represents the transverse movement of the cars chassis, especially
         * when dealing with crosswinds. The Z-Direction represents vertical
         * movement of the cars chassis when dealing with uneven road surfaces.
         */
        CoordinateSystem chassis;
        /*! \brief defines the 4 wheels which each have a coordinate system */
        std::array<Wheel, 4> wheels;
    public:
        /*! \brief returns the amount of wheels a car has
         *
         * Usually 4 unless defined differently
         */
        unsigned long getAmountWheels();
};

#endif // CAR_H_
