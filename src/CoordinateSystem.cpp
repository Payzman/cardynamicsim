#include "./CoordinateSystem.hpp"

TranslationalCoordinate CoordinateSystem::getTranslation(Direction dir) {
    return this->_translation[dir];
}

RotationalCoordinate CoordinateSystem::getRotation(Rotation rot) {
    return this->_rotation[rot];
}

void CoordinateSystem::setTranslation(Direction dir, double val) {
    this->_translation[dir] = val;
}

void CoordinateSystem::setRotation(Rotation rot, double val) {
    this->_rotation[rot] = val;
}

bool CoordinateSystem::operator==(CoordinateSystem* other) {
    return (this->_translation == other->_translation) && (this->_rotation == other->_rotation);
}

bool CoordinateSystem::operator!=(CoordinateSystem* other) {
    return !(this == other);
}
