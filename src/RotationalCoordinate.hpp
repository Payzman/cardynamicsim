#ifndef ROTATIONALCOORDINATE_H_
#define ROTATIONALCOORDINATE_H_

#include "./Coordinate.hpp"

/*! \brief Coordinates for rotational movement */
class RotationalCoordinate : public Coordinate {
    public:
        using Coordinate::operator=;
        using Coordinate::operator==;
};

#endif // ROTATIONALCOORDINATE_H_
