#ifndef TRANSLATIONALCOORDINATE_H_
#define TRANSLATIONALCOORDINATE_H_

#include "./Coordinate.hpp"

/*! \brief Coordinate for translational movement */
class TranslationalCoordinate : public Coordinate {
    public:
        using Coordinate::operator=;
        using Coordinate::operator==;
};

#endif // TRANSLATIONALCOORDINATE_H_
