#include "./Coordinate.hpp"

Coordinate::Coordinate() {
    this->_s = 0.0;
}

Coordinate::Coordinate(double s) {
    this->_s = s;
}

Coordinate& Coordinate::operator=(double rhs) {
    this->_s = rhs;
    return *this;
}

bool Coordinate::operator==(double other) {
    return fabs(this->_s - other) <= Coordinate::epsilon;
}

bool Coordinate::operator==(Coordinate other) const {
    return fabs(this->_s - other._s) <= Coordinate::epsilon;
}
