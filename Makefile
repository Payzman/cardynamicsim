.SUFFIXES:
.PHONY: checkstyle clean doc

EXECNAME = CarDynamicSim
CXX = clang++
CFLAGS = -Weverything -Wall -Wextra -g3 -std=c++11 -Wno-c++98-compat

TEST_SRC_FILE = ./test/$(EXECNAME)Test.cpp
TEST_OBJ_FILE = $(TEST_SRC_FILE:.cpp=.o)

MAIN_SRC_FILE = ./src/$(EXECNAME)Main.cpp
MAIN_OBJ_FILE = $(MAIN_SRC_FILE:.cpp=.o)

SRC_FILES = $(filter-out $(wildcard ./src/*Main.cpp), $(wildcard ./src/*.cpp))
OBJ_FILES = $(SRC_FILES:.cpp=.o)

LIBS =
TEST_LIBS = -lgtest -lgtest_main -lpthread

all: checkstyle test main doc

doc:
	doxygen Doxyfile

checkstyle:
	uncrustify -c default.cfg --no-backup ./src/*.cpp ./src/*.hpp ./test/*.cpp ./test/*.hpp

main: ./bin/$(EXECNAME)Main
	./bin/$(EXECNAME)Main

./bin/$(EXECNAME)Main: $(OBJ_FILES) $(MAIN_OBJ_FILE)
	$(CXX) $^ -o $@ $(LIBS)

test: ./bin/$(EXECNAME)Test
	./bin/$(EXECNAME)Test

./bin/$(EXECNAME)Test: $(OBJ_FILES) $(TEST_OBJ_FILE)
	$(CXX) $^ -o $@ $(LIBS) $(TEST_LIBS)

%.o: %.cpp
	$(CXX) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.orig $(OBJ_FILES) $(TEST_OBJ_FILE) $(MAIN_OBJ_FILE) ./bin/$(EXECNAME)Test ./bin/$(EXECNAME)Main *.unc-backup*
